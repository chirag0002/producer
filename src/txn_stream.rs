extern crate rdkafka;
use rdkafka::producer::{FutureProducer, FutureRecord};
use rdkafka::util::Timeout;
use rdkafka::consumer::{BaseConsumer, Consumer};
use rdkafka::message::Message;

pub async fn data_in(producer: FutureProducer, txn_topic: &str, txn_value: &[u8], txn_key: &str) {

    let record = FutureRecord::to(txn_topic).payload(txn_value).key(txn_key);

    let timeout = Timeout::Never;

    producer.send(record, timeout).await.unwrap();
}

